<?php
namespace App\Bitm;
class Monitor{
    public $monitorBrandName="";
    public $monitorColor="";
    public $monitorPrice;
    public $monitorSize;
    public $monitorWeight;
    public $monitorDetails="";
    public function __construct($mBrandName,$mColor,$mPrice,$mSize,$mWeight)
    {
        $this->monitorBrandName=$mBrandName;
        $this->monitorColor=$mColor;
        $this->monitorPrice=$mPrice;
        $this->monitorSize=$mSize;
        $this->monitorWeight=$mWeight;
    }
    public function monitorDetails($msg){
        echo "<h3>$msg</h3>";
        $monitorDetails=array(
            '0'=>"Brand Name : ". $this->monitorBrandName,
            '1'=>"Color : ". $this->monitorColor,
            '2'=>"Price : ". $this->monitorPrice. " BDT",
            '3'=>"Size : ". $this->monitorSize. '"',
            '4'=>"Weight : ". $this->monitorWeight." kg",
        );
        return $monitorDetails;

    }
}
