<?php
namespace App\Bitm;
class Pc{
    public $pcBrandName="";
    public $pcColor="";
    public $pcPrice;
    public $pcWeight;
    public $pcDetails="";
    public function __construct($pcBrandName,$pcColor,$pcPrice,$pcWeight)
    {
        $this->pcBrandName=$pcBrandName;
        $this->pcColor=$pcColor;
        $this->pcPrice=$pcPrice;
        $this->pcWeight=$pcWeight;
    }
    public function pcDetails($msg){
        echo "<h3>$msg</h3>";
        $pcDetails=array(
            '0'=>"Brand Name : ". $this->pcBrandName,
            '1'=>"Color : ". $this->pcColor,
            '2'=>"Price : <strong>". $this->pcPrice. "</strong> BDT",
            '4'=>"Weight : ". $this->pcWeight." kg",
        );
        return $pcDetails;
    }
}

