<?php
include_once('vendor/autoload.php');
use App\Bitm\Monitor;
use App\Bitm\Pc;
$objMonitor=new Monitor("Dell","Black",12500,19,2.5);
echo "<h2>Call to Monitor Properties</h2>";
echo "<h5>Monitor Brand Name: $objMonitor->monitorBrandName";
echo "<h5>Monitor Color: $objMonitor->monitorColor";
echo "<h5>Monitor Price: $objMonitor->monitorPrice";
echo "<h5>Monitor Size: $objMonitor->monitorSize";
echo "<h5>Monitor Weight: $objMonitor->monitorWeight";
$mDetails=$objMonitor->monitorDetails("Call to Monitor Details Methode");
foreach ($mDetails as $index => $mDetail) {
    echo "$mDetail <br/>";
}
echo "==================================================<br/>";
$pcObj=new Pc('HP','Black',25000,4);
echo "<h2>Call to PC Properties</h2>";
echo "<h5>PC Brand Name: $pcObj->pcBrandName";
echo "<h5>PC Color: $pcObj->pcColor";
echo "<h5>PC Price: $pcObj->pcPrice";
echo "<h5>PC Weight: $pcObj->pcWeight";
$pcDetails=$pcObj->pcDetails("Call to Personal Computer Details Method");
foreach ($pcDetails as $index => $pcDetail) {
    echo "$pcDetail <br/>";
}